package com.leu.kongsun.retrofit;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface JsonPlaceHolderAPI {
    @GET("posts")
    Call<List<Post>> getPosts(
            @Query("userId") Integer[] userId,
            @Query("_sort") String sort,
            @Query("_order") String order
    );
    @GET("posts")
    Call<List<Post>> getPostsByQuery(@Query("userId") int id);
    @GET("comments")
    Call<List<Comment>> getComments(@Query("postId") int postId);
    @GET("posts/{id}/comments")
    Call<List<Comment>> getCommentsByPath(@Path("id") int id);
    @GET("posts")
    Call<List<Post>> getPostsByHashMap(@QueryMap Map<String,String> parameter);
    @GET("posts{userId}")
    Call<List<Post>> getPosts(@Query("userId") int userId);

    @FormUrlEncoded
    @POST("posts")
    Call<Post> createPost(@Field("userId") int userId,
                          @Field("title") String title,
                          @Field("body") String body
    );

    @PUT("posts/{id}")
    Call<Post> createPut(@Path("id") int id, Post post);

    @PATCH("posts/{id}")
    Call<Post> createPath(@Path("id") int id, Post post);

    @DELETE("posts/{id}")
    Call<Post> delete(@Path("id") int id, Post post);
}
